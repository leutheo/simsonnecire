-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 02 juil. 2020 à 05:36
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `simsonnecire`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `telephone` int(255) NOT NULL,
  `courriel` varchar(255) NOT NULL,
  `mot_passe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `prenom`, `telephone`, `courriel`, `mot_passe`) VALUES
(1, 'theo', 'theo', 2147483647, 'theo@gmail.com', '7f92067d614a922643319fb15d4b238a1e125543');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prix` double NOT NULL,
  `description` text NOT NULL,
  `date_livraison` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `adresse` varchar(255) NOT NULL,
  `cout_expedition` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id`, `nom`, `prix`, `description`, `date_livraison`, `adresse`, `cout_expedition`) VALUES
(1, 'Version de base', 1250.99, 'Bonne pour deux utilisations.\r\nElle attire 50 % des molécule atomique 15 pied autour de l\'utilisateur.\r\nPour plus de sécurité elle est impossible à modifier. Si la boite est ouverte\r\nelle explose.\r\nElle est blanche (tout le monde sait que la couleur ça coute cher)', '2020-07-01 21:55:10', ' 150-174 Rue Chênevert, Québec, QC G1K 1B1', 15),
(3, 'Version de pas base', 3050.74, 'Bonne pour deux utilisations.\r\nElle attire 25 % des molécule atomique 10 pied autour de l\'utilisateur.\r\nPour plus de sécurité elle est trempée dans un plastique qui la rend\r\nmonobloc et impénétrable.\r\nElle est de couleur pastel', '2020-07-01 21:55:10', 'FIX AUTO LIMOILOU, 150 Rue de l\'Espinay, Québec, QC G1L 2H6', 20),
(4, 'Version de gros gros luxe', 10287.23, 'Bonne pour 2 utilisations\r\nElle n\'attire aucune molécule nocive.\r\nPour plus de sécurité elle est trempée dans un plastique qui la rend\r\nmonobloc et impénétrable qui elle est recouverte d’une couche d’acier\r\npour plus d’impénétrabilité.\r\nDe plus une couverture de fonte permet de ne pas le perdre car au poids\r\nqu\'il a vous ne pouvez l\'oublier! (Plus c\'est lourd plus ça vaut cher non?).\r\nElle a deux choix de couleurs vive (tout le monde sait que la couleur ça\r\ncoute cher) Rouge ou brun vif.', '2020-07-01 21:55:10', 'Place des Chênes, 163 Rue des Chênes O #135, Quebec City, Quebec G1L 1K6', 30);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
