<?php 
include_once'./includes/functions/data/connecteur.php';

    $errors = null;

    // Lecture des valeurs du formulaire
    function form_values($nom){
        echo(isset($_POST[$nom]) ? $_POST : "");
    }


    // Validation des donnes pour l'inscription
    function validation_inscription(){
        $errors = [];
        $errors["nom"] = trim(htmlspecialchars(empty($_POST["nom"]) ? "La case du nom ne peut pas rester vide" : ""));
        $errors["prenom"] = trim(htmlspecialchars(empty($_POST["prenom"]) ? "La case du prenom ne peut pas rester vide" : ""));
        $errors["courriel"] = trim(htmlspecialchars(empty(validation_email($_POST["courriel"])) ? "La case du courriel ne peut pas rester vide" : ""));
        $errors["telephone"] = trim(htmlspecialchars(strlen($_POST["telephone"]) != 10 ? "La case du téléphone ne peut pas rester vide et doit contenir 10 chiffres" : ""));
        $errors["password"] = trim(htmlspecialchars(empty($_POST["password"] || ($_POST["password"] != $_POST["confirmpassword"])) ? "Les deux mots de passe doivent être identiques et avoir aumoins 8 caractères" : ""));
        
        if(count($errors) >= 1){
            return $errors;
        }

    }

    // Filtre Validation
    function validation_email($courriel){
        return filter_var($courriel, FILTER_VALIDATE_EMAIL);
    }

    function validation_phone($phone){
        return filter_var($phone, FILTER_VALIDATE_INT);
    }


    // Regex 
    function email_validation($str){
        return(!preg_match("^[a-z0-9-]+(\.[a-z0-9-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}$^", $str)) ? false : true;
    }

    function validation_telephone($telephone){
        return(!preg_match("/^([0-9]{3})-[0-9]{3}-[0-9]{4}$/", $telephone));
    }

    function validation_password($password){
        return(!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{8,}$#", $password));
    }

    // Assainissement des données


    // Méthode de requête POST
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        $form = validation_inscription();
        if (count($form) > 1){
            $errors = $form;
        }
        return $errors;
    }

?>

