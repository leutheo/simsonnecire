
<?php
//On définit les entrées
$server = "localhost"; //127.0.0.1
$dbname = "simsonnecire";
$user = "root";
$password = "";
$option = [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false,
];

//On établit la connexion
try {
    $bdd = new PDO("mysql:host=$server;dbname=$dbname", $user, $password);
} catch (PDOException $e) {
    // throw new PDOException($e->getMessage(),(int)$e->getCode());
    return $e->getMessage();
}
?>