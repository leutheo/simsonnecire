<?php
include_once'./includes/functions/data/connecteur.php';
include_once'./includes/parts/header.php'; 
?>
<main id="main">

  <section id="commande">
  <?php
      include_once'./includes/functions/suivi-commande2-function.php';
    ?>
    <h1>Suivi de la commande</h1>
    <div class="produits">
      <div class="baseImg">
        <img src="img/grosluxe.png" alt="base">
      </div>
      <div class="basebtn commande">
        <p><b>Nom du produit :</b><?php echo " ", $nom?></p>
        <p><b>Description du produit :</b><?php echo " ", $description?></p>
        <p><b>Numéro de la commande :</b><?php echo " ", $numerocommande?></p>
        <p><b>Date de l'ivraison prévue :</b> <?php echo " ", $datelivraison?></p>
        <p><b>Adresse de livraison :</b><?php echo " ", $adresse?></p>
        <p><b>Prix :</b><?php echo " ", $prix?>$</p>
        <p><b>Frais d'Expédition :</b><?php echo " ", $fraisexpedition?>$</p>
      </div>
    </div>
    <div class="details">
      <div id="base">
        <p><a href="index.php">Retour</a></p>
      </div>
    </div>
  </section>
</main> 
<?php
  include_once'./includes/parts/footer.php';
?>
