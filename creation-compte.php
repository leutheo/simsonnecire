<?php
    include_once'./includes/functions/data/connecteur.php';
    include_once'./includes/parts/header.php';
  ?>

<main id="container">
    <?php 
      include_once'./includes/functions/creationcompte-function.php';
    ?>

      <section id="formulaire" >
      <h1 class="form">Création de compte</h1>
      <form id="form-inscription" action="" method="POST">
        <fieldset id="field">
          <!-- <legend>Création de compte</legend> -->
          <p>
            <label for="nom">Nom :</label>
            <input class="inputLargeur" type="text" name="nom" id="nom" placeholder="nom" value="<?php if(isset($nom)) {echo $nom;}?>" >
          </p>
          <p>
            <label for="prenom">Prénom :</label>
            <input class="inputLargeur" type="text" name="prenom" id="prenom" placeholder="prénom" value="<?php if(isset($prenom)) {echo $prenom;}?>">
          </p>
          <p>
            <label for="courriel">Courriel :</label>
            <input class="inputLargeur" type="email" name="courriel" id="courriel" placeholder="example@mail.com" value="<?php if(isset($courriel)) {echo $courriel;}?>">
          </p>
          <p>
            <label for="numero">Téléphone :</label>
            <input class="inputLargeur" type="tel" name="telephone" id="telephone" placeholder="(000)-000-0000" value="<?php if(isset($numero)) {echo $numero;}?>">
          </p>
          <p>
            <label for="psw1">Mot de passe :</label>
            <input class="inputLargeur" type="password" id="pass" name="pass" placeholder="mot de passe"> 
          </p>
          <p>
            <label for="psw2">Confirmation mot de passe :</label>
            <input class="inputLargeur" type="password" id="confirmpass" name="confirmpass" placeholder="confirmez mot de passe" >
          </p>
        </fieldset>  
        <div id="bouton">
          <input class="bouton btn" type="reset" value="Effacer le formulaire" >
          <input class="bouton fr btn" type="submit" name="inscription" value="Soumettre votre formulaire">
        </div>     
        
      </form>
      <?php
        if(isset($erreur)){
          echo $erreur;  
        } 
      ?>
    </section>
</main>
  <?php
    include_once'./includes/parts/footer.php';
  ?>
 