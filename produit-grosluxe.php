<?php
include_once'./includes/parts/header.php'; 
?>
<main id="main">

<section id="base">
<?php
    include_once'./includes/functions/produit-grosluxe-function.php';
  ?>
  <h2><?php echo $nom, " ", $prix?>$</h2>
  <div class="produits">
    <div class="details">
      <ul>
        <li><?php echo $description?></li>
      </ul>
      <img src="img/grosluxe.png" alt="base">
      <div class="retour">
        <p><a href="liste-commande.php">Retour à la liste de mes commandes</a></p>
      </div>
    </div>
  </div>
</section>
</main>  

<?php
  include_once'./includes/parts/footer.php';
?>
