<?php
include_once'./includes/parts/header.php'; 
?>
<main id="main">
  <section id="banniere">
    <!-- <img src="img/cover.png" alt="cover"> -->
    <div id="haut">
      <div id="logo">
        <a href="#"><img src="img/logo.png" alt="logo" ></a>
      </div>

      <div id="marque">
        <h1>Simsonne Cire</h1>
        <p>Air frais en tout temps...</p>
      </div>
    </div>
  
    <div id="droite">
      <div id="bas">
        <div id="slogan">
          <h2>Sub-discombobulation Atomique</h2>
          <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition  et la mise en page avant impression.</p>
        </div>
        <div id="medias">
          <img src="img/icons/facebook.svg" alt="facebook">
          <img src="img/icons/instagram.svg" alt="instagram">
          <img src="img/icons/twitter.svg" alt="twitter">
          <img src="img/icons/whatsapp.svg" alt="whatsapp">
        </div>
      </div>
    </div>
  </section>

  <section id="base">
  <?php
      include_once'./includes/functions/produit-base-function.php';
    ?>
    <!-- <h2>Version de base 1250,99$</h2> -->
    <h2><?php echo $nom, " ", $prix?>$</h2>
    <div class="produits">
      <div class="baseImg">
        <img src="img/base.png" alt="base">
      </div>
      <div class="basebtn version">
        <p><a href="liste-commande.php">Réservez ce produit</a></p>
        <p><a href="creation-compte.php">Créez votre compte</a></p>
        <p><a href="suivi-commande.php">Suivre une commande</a></p>
        <input type="checkbox" id="deBase" name="deBase">
        <label for="deBase">Inscrivez-vous à l’infolettre</label>
      </div>
    </div>
  </section>

  <section id="pasbase">
  <?php
      include_once'./includes/functions/produit-pasbase-function.php';
    ?>
    <h2><?php echo $nom, " ", $prix?>$</h2>
    <!-- <h2>Version de base 3050,74$</h2> -->
    <div class="produits">
      <div class="basebtn version special">
        <p><a href="liste-commande.php">Réservez ce produit</a></p>
        <p><a href="creation-compte.php">Créez votre compte</a></p>
        <p><a href="suivi-commande1.php">Suivre une commande</a></p>
        <input type="checkbox" id="pasdeBase" name="pasdeBase">
        <label for="pasdeBase">Inscrivez-vous à l’infolettre</label>
      </div>
      <div class="baseImg">
        <img src="img/pasbase.png" alt=" pas base">
      </div>
    </div>
  </section>

  <section id="grosLuxe">
  <?php
      include_once'./includes/functions/produit-grosluxe-function.php';
    ?>
    <h2><?php echo $nom, " ", $prix?>$</h2>
    <!-- <h2>Version de gros gros luxe 10287,23$</h2> -->
    <div class="produits">
      <div class="baseImg">
        <img src="img/grosLuxe.png" alt="gros Luxe">
      </div>
      <div class="basebtn version">
        <p><a href="liste-commande.php">Réservez ce produit</a></p>
        <p><a href="creation-compte.php">Créez votre compte</a></p>
        <p><a href="suivi-commande2.php">Suivre une commande</a></p>
        <input type="checkbox" id="grosdeLuxe" name="grosLuxe">
        <label for="grosLuxe">Inscrivez-vous à l’infolettre</label>
      </div>
    </div>
  </section>



</main> 

<?php
  include_once'./includes/parts/footer.php';
?>
